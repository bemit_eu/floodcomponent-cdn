<?php

namespace Flood\Component\Cdn\Route;

use Flood\Component\Route\Container as RouteContainer;
use Flood\Component\Route\Generator\HandlerInterface;
use Flood\Component\Route\Route;
use Hydro\Container;
use Symfony\Component\Routing\Route as SymRoute;
use Symfony\Component\Routing\RouteCollection;

class File implements HandlerInterface {

    public $data;

    /**
     * @param array $data
     */
    public function add($data) {
        $this->data = $data;
    }

    public function get() {
        return $this->data;
    }

    /**
     * @param string $hook_id
     *
     * @return \Symfony\Component\Routing\RouteCollection
     * @todo implement route cache
     *
     * @todo remove hydro container and implement binding CDN container
     */
    public function generateByHook($hook_id) {
        $collection = new RouteCollection();

        foreach(RouteContainer::_hookStorage()->get($hook_id)->getStorageObject('file')->get() as $key => $data) {

            if((defined('FLOOD_COMPONENT_ROUTE__USE_DEV') && FLOOD_COMPONENT_ROUTE__USE_DEV) || PHP_SAPI === 'cli-server') {
                // todo: refine dev server url
                $protocol = 'HTTP';
                $host = Route::i()->devReplaceHost(Container::_cdn()::$config->buildHost());
            } else {
                $protocol = Container::_cdn()::$config->protocol();
                $host = Container::_cdn()::$config->buildHost();
            }

            if(Route::$debug) {
                echo 'Added CDN File: ' . "\r\n" .
                    'uri:        ' . $data['uri'] . "\r\n" .
                    'name-key:   ' . $key . (!empty($data['tag']) ? '|' . $data['tag'] : '') . "\r\n" .
                    'path:       ' . $data['uri'] . "\r\n" .
                    'host:       ' . $host . "\r\n" .
                    'schemes:    ' . $protocol . "\r\n" . "\r\n";
            }

            $collection->add(
            // `name` must be unique among all generated routes
                $key . (!empty($data['tag']) ? '|' . $data['tag'] : ''),
                new SymRoute(
                    ($data['uri']),
                    [
                        'any' => '',
                        'hook' => $hook_id,
                        '_controller' => function() use ($data) {
                            Container::_cdn()->displayFile($data);
                        },
                    ],//defaults
                    ['any' => '.*'],//requirements
                    [],// (isset($response['options']) ? $response['options'] : []),
                    (!empty($host) ? $host : ''),
                    (!empty($protocol) ? ['HTTPS' , 'HTTP'] : [])
                // (isset($response['methods']) ? $response['methods'] : []),
                // (isset($response['condition']) ? $response['condition'] : '')
                )
            );
        }

        return $collection;
    }

    public function generateByHookLocale($hook_id, $locale) {
        // not used
        return new RouteCollection();
    }
}