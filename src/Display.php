<?php

namespace Flood\Component\Cdn;

use Hydro\Container;

/**
 * Handler class for a single display execution, will send header, data and more
 *
 * @category
 * @package    Flood\Component\Cdn
 * @author     Original Author michael@bemit.codes
 * @link
 * @copyright  2017 - 2018 Michael Becker
 * @since      Version 0.3
 * @version    Release: @package_version@
 */
class Display {
    /**
     * @var null|array per-route configuration, is injected at route registration
     */
    protected $data = null;

    public function __construct($data) {
        $this->data = $data;
    }

    /**
     * @param $path
     *
     * @return null|string
     */
    public function getMimeType($path) {
        $mime = null;
        foreach(Container::_cdn()::$config->extensionToMime() as $extension => $tmp_mime) {
            if(false !== strpos(strrev($path), strrev($extension))) {
                $mime = $tmp_mime;
                break;
            }
        }
        if(!is_string($mime)) {
            // fallback when not configured, but this function is not very trustable (e.g. css is text/plain)
            $mime = mime_content_type($path);
        }

        return $mime;
    }

    /**
     * Sends needed headers for the current file, will be depending on configuration
     * Supports:
     * - 304 state 'not modified'
     * - etag header with md5 file contents
     * - 304 state `strict` and not strict
     * - mime type out of file extension/custom mimetypes
     * - force download of files/attachment
     * - cache control
     * - expires header will
     *
     * @param $file
     * @param $mime
     */
    public function sendHeader($file, $mime) {
        $etag = md5_file($file);
        $file_time = filemtime($file);

        $this->header('Access-Control-Allow-Origin: *');
        if(isset($this->data['cache']['last_mod_send']) && $this->data['cache']['last_mod_send']) {
            $this->header('Last-Modified: ' . gmdate('D, d M Y H:i:s', $file_time) . ' GMT');
        }
        if(isset($this->data['cache']['etag_send']) && $this->data['cache']['etag_send']) {
            $this->header('Etag: ' . $etag);
        }

        if(isset($this->data['cache']['control']) && $this->data['cache']['control']) {
            $this->header('Cache-control: ' . $this->data['cache']['control']);
        }
        if(isset($this->data['cache']['expires']) && $this->data['cache']['expires']) {
            $this->header('Expires: ' . gmdate('D, d M Y H:i:s', time() + $this->data['cache']['expires']) . ' GMT');
        }

        // if should be checked for the `304 header`
        if(isset($this->data['cache']['304_check']) && $this->data['cache']['304_check']) {
            $strict = false;
            if(isset($this->data['cache']['304_check_strict']) && $this->data['cache']['304_check_strict']) {
                $strict = true;
            }

            $valid_last_mod = false;
            if(filter_has_var(INPUT_SERVER, 'HTTP_IF_MODIFIED_SINCE') && filter_input(INPUT_SERVER, 'HTTP_IF_MODIFIED_SINCE', FILTER_SANITIZE_STRING) === gmdate('D, d M Y H:i:s', $file_time) . ' GMT') {
                $valid_last_mod = true;
            }
            $valid_etag = false;
            if(filter_has_var(INPUT_SERVER, 'HTTP_IF_NONE_MATCH') && filter_input(INPUT_SERVER, 'HTTP_IF_NONE_MATCH', FILTER_SANITIZE_STRING) === $etag) {
                $valid_etag = true;
            }

            if(
                ($strict && $valid_etag && $valid_last_mod) ||
                (!$strict && ($valid_etag || $valid_last_mod))
            ) {
                // clause which will check if the 304 header should only be sended when both etag and last mod matches
                // when up to date, send 304 and exit
                $this->header('HTTP/1.1 304 Not Modified');
                exit();
            }
        }

        $this->header('Content-type: ' . $mime);

        if(isset($this->data['content-disposition'])) {
            /**
             * `content-disposition`: tells if file should be displayed or a download is forced, `content-disposition.value` = `inline` = display in browser, `content-disposition.value` = `attachment` = download forced
             */
            // todo add exception/isset's
            if(false !== strpos($this->data['content-disposition']['filename'], '{FILE}')) {
                $file_name = str_replace($this->data['content-disposition']['filename'], '{FILE}', $file);
            } else {
                $file_name = $this->data['content-disposition']['filename'];
            }

            $this->header('Content-Disposition: ' . $this->data['content-disposition']['value'] . '; filename=' . basename($file_name));
        }
    }

    /**
     * Final display of one exact file
     *
     * @param $file_path
     */
    public function display($file_path) {
        // make absolute path to file
        $file_path = Container::_cdn()::$config->serverPath() . $file_path;

        if(is_file($file_path)) {
            $mime = $this->getMimeType($file_path);
            //$info = getimagesize($this->server_path);// fetch some file information

            $output = [];
            if(Container::_cdn()::$config->isBufferRemovePrev($mime)) {
                ob_end_clean();
                ob_start();
            } else if(Container::_cdn()::$config->isBuffer($mime)) {
                if(0 < ob_get_status(true)[0]['buffer_used']) {
                    $output[] = ob_get_contents();
                    ob_end_clean();
                    ob_start();
                }
            }

            $this->sendHeader($file_path, $mime);

            // checking for enabled and/or configured performance mod's
            $mod_run = false;
            if(false !== Container::_cdn()::$config->mod_enabled) {
                // todo: must be implemented
                $mod_run = $this->displayThroughMod($file_path, $output);
            }

            $php_run = false;
            if(!$mod_run) {
                // when successful displayed through a mod, execute the php display method
                $php_run = $this->displayThroughPhp($file_path, $mime, $output);
            }

            if(Container::_cdn()::$config->isBuffer($mime) && Container::_cdn()::$config->isTemplate($mime)) {
                // todo add custom template config possibility
                $output[] = '<html style="padding:0;margin:0;width:100%;"><body style="padding:0;margin:0;"><img src="data:' . $mime . ';base64,' . base64_encode(ob_get_contents()) . '"></body></html>';
                ob_end_clean();
                ob_start();
            } else if(Container::_cdn()::$config->isBuffer($mime)) {
                $output[] = ob_get_contents();
                ob_end_clean();
                ob_start();
            }

            // end of any buffers, now display them
            // when no buffer was used the array is empty, but not null, so doesn't need to check as no run when no buffer :)
            foreach($output as $o) {
                echo $o;
            }

            if(!$mod_run && !$php_run) {
                // todo add 404 handler
                $this->header($_SERVER['SERVER_PROTOCOL'] . ' 404 Not Found');
                echo "\r\n" . '404: CDN File not displayable' . "\r\n";
            }
        } else {
            // todo add 404 handler
            $this->header($_SERVER['SERVER_PROTOCOL'] . ' 404 Not Found');
            echo "\r\n" . '404: CDN File not found' . "\r\n";
        }
    }

    /**
     * @param $file
     * @param $mime
     * @param $output
     *
     * @return bool
     */
    public function displayThroughPhp($file, $mime, &$output) {
        //var_dump($this->server_path);
        //var_dump($this->server_path . $file);
        readfile($file);

        return true;
    }

    /**
     * Displays the file through a performance enhanced module
     *
     * @param $path
     * @param $output
     *
     * @return bool
     * @todo: implement apache and nginx performance optimizing mod support
     *
     */
    public function displayThroughMod($path, &$output) {
        $mod_run = false;
        if('apache' == Container::_cdn()::$config->mod_enabled) {
            // todo finish apache implementation
            if(in_array('mod_xsendfile', apache_get_modules())) {
                $this->header('X-Sendfile: ' . $path);
                $mod_run = true;
            } else {
                error_log('Warning! mod-xsendfile is NOT INSTALLED');
            }
        } else if('nginx' == Container::_cdn()::$config->mod_enabled) {
            $mod_run = true;
            // todo implement nginx support
        } else if(filter_has_var(INPUT_SERVER, Container::_cdn()::$config->mod_enabled)) {
            $mod_run = true;
            // todo implement server key, must be again with a switch for apache and nginx
        }

        return $mod_run;
    }

    protected function header($header) {
        if(\Flood\Component\Cdn\CDN::$debug) {
            echo $header . "\r\n";
        } else {
            header($header);
        }
    }
}
