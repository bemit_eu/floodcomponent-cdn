<?php

namespace Flood\Component\Cdn;

/**
 * Config connector for this component.
 *
 * @category
 * @package    Flood\Component\Cdn
 * @author     Original Author michael@bemit.codes
 * @link
 * @copyright  2017 - 2018 Michael Becker
 * @since      Version 0.3
 * @version    Release: @package_version@
 */
class Config {

    /**
     * @var array configuration data as an array, added through `set`, available through `get(<key>)`
     */
    protected $data;

    /**
     * @var bool|string possible is false, or any string, when string is `nginx` it will force nginx accell modul, when `apache` it will force apache xsendfile mod, when other string will check if server value $_SERVER exists with string as key
     *                  For explicit check set it in apache like:
     *                  <IfModule mod_xsendfile.c>
     *                      <Files *.php>
     *                          XSendFile On
     *                          XSendFileAllowAbove On
     *                          SetEnv MOD_X_SENDFILE_ENABLED 1
     *                      </Files>
     *                  </IfModule>
     *                  For nginx:
     *                  todo add nginx support
     */
    public $mod_enabled = false;

    /**
     * @param $data
     */
    public function addData($data) {
        $this->data = $data;
    }

    public function get($var_key = null) {
        return $this->data[$var_key];
    }

    public function set($data, $var_key) {
        return $this->data[$var_key] = $data;
    }

    /**
     * Returns the absolute path to project root, not this component root
     *
     * @param string|null $server_path with trailing slash
     *
     * @return string
     */
    public function serverPath($server_path = null) {
        if(null !== $server_path) {

            $this->set($server_path, 'server-path');

            return $this->get('server-path');
        } else {
            return $this->get('server-path');
        }
    }

    /**
     * Returns the protocol that will be enforced, e.g. `https`, `http`
     *
     * @return string
     */
    public function protocol() {
        return $this->get('uri')['protocol'];
    }

    /**
     * @return string
     */
    public function domain() {
        return $this->get('uri')['domain'];
    }

    /**
     * @return string
     */
    public function path() {
        return $this->get('uri')['path'];
    }

    /**
     * @return array
     */
    public function output() {
        return $this->get('output');
    }

    /**
     * @return string
     */
    public function outputBufferRemovePrev() {
        return $this->output()['buffer-remove-prev'];
    }

    public function outputBufferRemovePrevFilter() {
        return $this->output()['buffer-remove-prev-filter'];
    }

    public function outputBuffer() {
        return $this->output()['buffer'];
    }

    public function outputBufferFilter() {
        return $this->output()['buffer-filter'];
    }

    public function outputTemplate() {
        return $this->output()['template'];
    }

    public function outputTemplateFilter() {
        return $this->output()['template-filter'];
    }

    public function extensionToMime() {
        return $this->get('extension-to-mime');
    }

    /**
     * Checks if the given mime type is in the filter array for the buffer
     *
     * @param string $mime
     *
     * @return bool
     */
    public function isBuffer($mime) {
        $return_val = false;
        if($this->outputBuffer()) {
            if(is_array($this->outputBufferFilter()) && in_array($mime, $this->outputBufferFilter())) {
                $return_val = true;
            }
        }

        return $return_val;
    }

    /**
     * Checks if the given mime type is in the filter array for the buffer
     *
     * @param string $mime
     *
     * @return bool
     * @todo hardcoded false, implement correct usage in display()
     *
     */
    public function isTemplate($mime) {
        $return_val = false;
        if($this->outputTemplate()) {
            if(is_array($this->outputTemplateFilter()) && in_array($mime, $this->outputTemplateFilter())) {
                $return_val = false;
            }
        }

        return $return_val;
    }

    /**
     * Checks if the given mime type is in the filter array for the buffer
     *
     * @param string $mime
     *
     * @return bool
     */
    public function isBufferRemovePrev($mime) {
        $return_val = false;
        if($this->outputBufferRemovePrev()) {
            if(is_array($this->outputBufferRemovePrevFilter()) && in_array($mime, $this->outputBufferRemovePrevFilter())) {
                $return_val = true;
            }
        }

        return $return_val;
    }

    public function buildHost() {

        if(!empty($this->domain()) && '*' !== $this->domain()) {
            $domain = $this->domain();
        } else {
            $domain = '';
            // when domain is not set, also the protocol must not be set
        }

        return $domain;
    }
}