<?php

namespace Flood\Component\Cdn;

use Flood\Component\Route as FloodRoute;
use Flood\Component\Sonar\Annotation\Service as Service;

/**
 * CDN - json config based routing for file and folder serving
 *
 * @Service(id="cdn", executed=false)
 * @category
 * @package    Flood\Component\Cdn
 * @author     Original Author michael@bemit.codes
 * @link
 * @copyright  2017 - 2018 Michael Becker
 * @since      Version 0.3
 * @version    Release: @package_version@
 */
class CDN {
    public static $debug;

    /**
     * @var array $route used to store all routing, from path to path or file to file, routing hide the actually position of data and serve it from another url
     */
    protected $route = [];

    /**
     * @var Config $config
     */
    public static $config = null;

    public function __construct() {
        if(null === static::$config) {
            static::$config = new Config();
        }

        $this->route = [
            'folder' => [
                /*
                 * 'from' => 'to'
                 * where from is the file on disc and to is the base folder which is accessible through CDN
                 */
            ],
            'file' => [
                /*
                 * 'from' => 'to'
                 * where from is the file on disc and to is alias url which is accessible through CDN
                 */
            ],
            'tag' => [
                /*
                 * 'tag' => ['from' => 'to']
                 * tag could be used with CDN->get() to fetch the to uri
                 * where from is the file on disc and to is alias url which is accessible through CDN
                 */
            ],
        ];
    }

    /**
     * The main frontend controller for displaying a folder that is CDN enabled, this means the route matched through a relative folder path and not a full path to the file e.g. `/asset/media/` instead of `/asset/media/img.jpg`, so it does not list/displays
     * the content of a folder but a file inside a folder
     *
     * @param $data
     *
     * @todo refactor tag selector, implement url path selector
     */
    public function displayFolder($data) {
        /**
         * @var Route\Folder $folder_obj get the object that has the stored information of the currently called folder
         */
        $folder_obj = FloodRoute\Container::_hookStorage()->get(FloodRoute\Route::i()->match['hook'])->getStorageObject('folder');

        /**
         * @var string $path_file relative server path to the storage file
         */
        $path_file = '';
        if(isset($data['tag'])) {
            foreach($folder_obj->data as $path => $ident_list) {
                if(isset($ident_list['tag']) && $ident_list['tag'] === $data['tag']) {
                    $path_file = $path;
                }
            }
        }

        $path_file .= '/' . FloodRoute\Route::i()->match['any'];

        $this->display($path_file, $data);
    }

    /**
     * @param $data
     *
     * @todo refactor tag selector, implement url path selector
     */
    public function displayFile($data) {
        /**
         * @var Route\File $file_obj get the object that has the stored information of the currently called hook
         */
        $file_obj = FloodRoute\Container::_hookStorage()->get(FloodRoute\Route::i()->match['hook'])->getStorageObject('file');
        /**
         * @var string $path_file relative server path to the storage file
         */
        $path_file = '';
        if(isset($data['tag'])) {
            foreach($file_obj->data as $path => $ident_list) {
                if(isset($ident_list['tag']) && $ident_list['tag'] === $data['tag']) {
                    $path_file = $path;
                }
            }
        }

        $this->display($path_file, $data);
    }

    public function display($path_file, $data) {
        $dsply = new Display($data);
        $dsply->display($path_file);
    }
}